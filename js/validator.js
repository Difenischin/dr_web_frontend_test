function validateForm(){

    if( !validateLogin()) {
        return false;
    }       
    if( !validatePassword()) {
        return false;
    }   

    $('.text-input-error').remove();
    $('.input-error').removeClass('input-error');

    return true;
}
function validateLogin() {
    var login = getLogin();

    if( login == "") {
        setError('input[name=email]',"Please enter your e-mail address.");
        return false;
    }
    if( !isValidEmail( login)) {
        setError('input[name=email]',"Please enter a valid e-mail address.");
        return false;
    }
    return true;
}
function getLogin() {
    return $('input[name=email]').val();
}

function validatePassword() {
    var password = getPassword();

    if( password == ""){
        setError('input[name=password]',"Please enter your password.");
        return false;
    }

    if(!isValidPassword( password)){
        setError('input[name=password]',"Password should contain 6-30 English letters, numbers, or special characters.");
        return false;
    }
    return true;
}
function getPassword() {
    return $('input[name=password]').val();
}

function setError(field,text) {
    $('.text-input-error').remove();
    $('.input-error').removeClass('input-error');

    if (($(field).attr('type')=='text') || ($(field).attr('type')=='password')) {
        $(field).addClass("input-error");
        $(field).after('<div class="text-input-error">'+text+'</div>');
    }
}

function isValidEmail(str)
{
    str=AtTrim(str);
    var apos = str.indexOf("@");
    var dpos = str.lastIndexOf(".");
    var spos = str.indexOf(" ");
    var cpos = str.indexOf(",");
    var ddpos = str.lastIndexOf("..");

    if (cpos>=0 || spos>=0 || apos<=0 || dpos<=0 || ddpos>=0 )
       return false;
   if(dpos<=apos+1) return false;
   if(str.charAt(apos+1)=='.') return false;
   if(str.charAt(str.length-1)=='.') return false;

   return true;
}

function AtTrim( str)
{
    var regExp1, regExp2, s1, s2, out;

    regExp1 = new RegExp("^ *");
    regExp2 = new RegExp(" *$");

    s1 = ""+str+"";
    s2 = s1.replace( regExp1, "");
    out = s2.replace( regExp2, "");
    
    regExp1 = null; 
    regExp2 = null;

    return( out);
}

function isValidPassword(str)
{
    var s = str.toLowerCase();
    var c;
    for (i=0;i< s.length;i++){
        c = s.charAt(i);
        if (c==' ') 
            return false;
    }
    if (i<6 || i>30)
        return false;
    return true;
}


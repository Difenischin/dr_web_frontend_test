function loginedUser() {
    if( !isLoginExist( getLogin())) {
        setError('input[name=email]',"Please enter a valid e-mail address.");
        return false;
    }
    if(!isTryPassword( getPassword())){
        setError('input[name=password]',"Wrong password");
        return false;
    }
    $('.section-login').hide();
    $( '<section class="logged-in"><div style="text-align: center;">  <h1>Welcome, you are logged in</h1> <a id="logout" class="login-btn" href="#" style="width: 125px;    margin: 0 auto;">Logout</a></div><section> ' ).insertAfter( ".section-login" );
}
function isLoginExist( login) {
    return (window.localStorage[login] != null);
}
function isTryPassword( password) {
    return getTryPassword() == password;
}    
function getTryPassword() {
    return localStorage[getLogin()];
}
function registerNewUser()
{
	var login = getLogin();
	if( isLoginExist( login)) {
		setError('input[name=email]',"This email exists in the system.");
        return false;
    }
    else{
        var password = getPassword();
        saveNewUser( login, password);
        
        $('.section-login').hide();
        $( '<section class="logged-in"><div style="text-align: center;">  <h1>Welcome you registered in the system</h1> <a id="logout" class="login-btn" href="#" style="width: 125px;    margin: 0 auto;">Logout</a></div><section> ' ).insertAfter( ".section-login" );
    }
}
function saveNewUser( login, password) {
	window.localStorage[login] = password;
}